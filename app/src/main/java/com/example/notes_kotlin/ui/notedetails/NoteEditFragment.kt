package com.example.notes_kotlin.ui.notedetails

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.notes_kotlin.R
import com.example.notes_kotlin.core.Resource
import com.example.notes_kotlin.data.local.AppDatabase
import com.example.notes_kotlin.data.local.LocalDataSource
import com.example.notes_kotlin.data.model.Note
import com.example.notes_kotlin.data.remote.ApiClient
import com.example.notes_kotlin.data.remote.NoteDataSource
import com.example.notes_kotlin.databinding.FragmentNoteEditBinding
import com.example.notes_kotlin.databinding.FragmentNotesBinding
import com.example.notes_kotlin.presentation.NoteViewModel
import com.example.notes_kotlin.presentation.NoteViewModelFactory
import com.example.notes_kotlin.repository.NoteRepositoryImp
import com.example.notes_kotlin.ui.notes.adapters.NoteAdapter

class NoteEditFragment : Fragment(R.layout.fragment_note_edit) {

    private lateinit var binding: FragmentNoteEditBinding

    private val viewModel by viewModels<NoteViewModel> {
        NoteViewModelFactory(NoteRepositoryImp(
            LocalDataSource(AppDatabase.getDataBase(this.requireContext()).noteDao()),
            NoteDataSource(ApiClient.service)))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentNoteEditBinding.bind(view)

        binding.btnAddNote.setOnClickListener{

            var note = Note(0,binding.editTitle.text.toString() , binding.textTitle.text.toString(),
                binding.editImageUrl.text.toString())

            viewModel.saveNote(note).observe(viewLifecycleOwner, Observer { result ->

                when(result){
                    is Resource.Loading -> {
                        binding.progressbar.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        binding.progressbar.visibility = View.GONE

                        findNavController().popBackStack()

                    }
                    is Resource.Failure -> {
                        binding.progressbar.visibility = View.GONE
                        Toast.makeText(requireContext(), "${result.exception.toString()}", Toast.LENGTH_LONG).show()
                    }
                }
            })
        }
    }

}