package com.example.notes_kotlin.ui.notes

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.notes_kotlin.R
import com.example.notes_kotlin.core.Resource
import com.example.notes_kotlin.data.local.AppDatabase
import com.example.notes_kotlin.data.local.LocalDataSource
import com.example.notes_kotlin.data.model.Note
import com.example.notes_kotlin.data.remote.ApiClient
import com.example.notes_kotlin.data.remote.NoteDataSource
import com.example.notes_kotlin.databinding.FragmentNotesBinding
import com.example.notes_kotlin.presentation.NoteViewModel
import com.example.notes_kotlin.presentation.NoteViewModelFactory
import com.example.notes_kotlin.repository.NoteRepositoryImp
import com.example.notes_kotlin.ui.notes.adapters.NoteAdapter

class NotesFragment : Fragment(R.layout.fragment_notes) {

    private lateinit var binding:FragmentNotesBinding
    private lateinit var  adapter:NoteAdapter

    private val viewModel by viewModels<NoteViewModel> {
        NoteViewModelFactory(NoteRepositoryImp(
            LocalDataSource(AppDatabase.getDataBase(this.requireContext()).noteDao()),
            NoteDataSource(ApiClient.service)))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentNotesBinding.bind(view)

        binding.recyclerNotes.layoutManager = GridLayoutManager(requireContext(),2)

        binding.swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light);

        binding.btnAddNote.setOnClickListener{
            val action = NotesFragmentDirections.actionNotesFragmentToNoteEditFragment()
            findNavController().navigate(action)
        }

        binding.swipeContainer.setOnRefreshListener {

            binding.recyclerNotes.adapter = null

            viewModel.fetchNotes().observe(viewLifecycleOwner, Observer { result ->
                when(result){
                    is Resource.Loading -> {
                        //binding.progressbar.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        //binding.progressbar.visibility = View.GONE

                        adapter = NoteAdapter(result.data.data){ note ->
                            onNoteClick(note)

                        }

                        binding.recyclerNotes.adapter = adapter

                        Log.d("LiveData","${result.data.toString()}")
                        binding.swipeContainer.setRefreshing(false)
                    }
                    is Resource.Failure -> {
                        binding.swipeContainer.setRefreshing(false)
                        Log.d("LiveData","${result.exception.toString()}")
                    }
                }
            })


        }

        viewModel.fetchNotes().observe(viewLifecycleOwner, Observer { result ->

            when(result){
                is Resource.Loading -> {
                    binding.progressbar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    binding.progressbar.visibility = View.GONE

                    adapter = NoteAdapter(result.data.data){ note ->
                        onNoteClick(note)
                    }

                    binding.recyclerNotes.adapter = adapter

                    Log.d("LiveData", "${result.data.toString()}")
                }
                is Resource.Failure -> {
                    binding.progressbar.visibility = View.GONE
                    Log.d("LiveData", "${result.exception.toString()}")
                }
            }
        })
    }

    private fun onNoteClick(note:Note){
        val action = NotesFragmentDirections.actionNotesFragmentToNoteDetailFragment(
            note.id,
            note.tittle,
            note.content,
            note.image
        )

        findNavController().navigate(action)
    }

}