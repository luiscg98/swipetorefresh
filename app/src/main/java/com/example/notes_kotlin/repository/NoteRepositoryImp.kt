package com.example.notes_kotlin.repository

import com.example.notes_kotlin.data.local.LocalDataSource
import com.example.notes_kotlin.data.model.Note
import com.example.notes_kotlin.data.model.NoteList
import com.example.notes_kotlin.data.remote.NoteDataSource
import com.example.notes_kotlin.data.model.toNoteEntity

class NoteRepositoryImp(
    private val localDataSource: LocalDataSource,
    private val dataSource: NoteDataSource): NoteRepository {
    override suspend fun getNotes(): NoteList {
        dataSource.getNotes().data.forEach { note ->
            localDataSource.saveNote(note.toNoteEntity())
        }

        return  localDataSource.getNotes()
    }

    override suspend fun saveNote(note: Note?): Note? {
        return dataSource.saveNote(note)
    }

    //override suspend fun getNotes(): NoteList = dataSource.getNotes()
    //override suspend fun saveNote(note: Note?): Note? =  dataSource.saveNote(note)
}