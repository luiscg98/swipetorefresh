package com.example.notes_kotlin.repository

import com.example.notes_kotlin.data.model.Note
import com.example.notes_kotlin.data.model.NoteList

interface NoteRepository {
    suspend fun getNotes(): NoteList
    suspend fun saveNote(note: Note?) : Note?
}