package com.example.notes_kotlin.data.model

fun List<NoteEntity>.toNoteList(): NoteList{
    val list = mutableListOf<Note>()

    this.forEach{ noteEntity ->

        list.add(noteEntity.toNote())

    }

    return NoteList(list)
}

fun Note.toNoteEntity():NoteEntity = NoteEntity(this.id, this.tittle, this.content, this.image)

fun NoteEntity.toNote(): Note = Note(this.id, this.tittle, this.content, this.imageUrl)