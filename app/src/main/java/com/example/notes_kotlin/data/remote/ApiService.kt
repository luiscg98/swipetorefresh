package com.example.notes_kotlin.data.remote

import com.example.notes_kotlin.data.model.Note
import com.example.notes_kotlin.data.model.NoteList
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {
    //https://testapi.io/api/luiscg98/resource/notes
    @GET("notes")
    suspend fun getNotes(): NoteList

    @POST("notes")
    suspend fun saveNote(@Body note: Note?) : Note?
}