package com.example.notes_kotlin.data.remote

import com.example.notes_kotlin.data.model.Note
import com.example.notes_kotlin.data.model.NoteList

class NoteDataSource (private val apiService: ApiService) {

    /*suspend fun getNotes(): NoteList
    {
        return apiService.getNotes()
    }*/

    suspend fun getNotes(): NoteList = apiService.getNotes()

    suspend fun saveNote(note:Note?) : Note? = apiService.saveNote(note)
}