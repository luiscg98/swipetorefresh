package com.example.notes_kotlin.data.model

data class Note(
    val id:Int = 0,
    val tittle:String = "",
    val content:String = "",
    val image:String = ""
)