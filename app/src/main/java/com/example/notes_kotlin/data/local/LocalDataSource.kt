package com.example.notes_kotlin.data.local

import android.content.Context
import androidx.room.Room
import com.example.notes_kotlin.data.model.Note
import com.example.notes_kotlin.data.model.NoteEntity
import com.example.notes_kotlin.data.model.NoteList
import com.example.notes_kotlin.data.model.toNoteList

class LocalDataSource( private  val noteDao: NoteDao) {
    suspend fun getNotes(): NoteList = noteDao.getNotes().toNoteList()

    suspend fun saveNote(note: NoteEntity) = noteDao.saveNote(note)
}