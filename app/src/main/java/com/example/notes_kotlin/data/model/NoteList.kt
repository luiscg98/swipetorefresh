package com.example.notes_kotlin.data.model

data class NoteList(val data:List<Note> = listOf())